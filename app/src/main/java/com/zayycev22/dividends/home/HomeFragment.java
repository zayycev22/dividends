package com.zayycev22.dividends.home;

import android.icu.util.Calendar;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.Toast;

import com.zayycev22.dividends.R;
import com.zayycev22.dividends.api.ApiWorker;
import com.zayycev22.dividends.api.data.Company;
import com.zayycev22.dividends.api.data.User;
import com.zayycev22.dividends.api.user.interfaces.OnRequestCompleted;
import com.zayycev22.dividends.consts.ApiConsts;
import com.zayycev22.dividends.databinding.FragmentHomeBinding;
import com.zayycev22.dividends.home.Calendar.CalendarData;
import com.zayycev22.dividends.home.Calendar.DateListener;

import org.threeten.bp.DayOfWeek;

import java.util.ArrayList;


public class HomeFragment extends Fragment implements OnRequestCompleted {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    private CalendarData calendarDataFragment;
    public DayOfWeek day = DayOfWeek.MONDAY;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private @NonNull FragmentHomeBinding binding;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private ApiWorker worker;
    private PopupView vv;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        binding = FragmentHomeBinding.inflate(inflater, container, false);
        CalendarView calendarView = binding.calendarView;
// Maximum available date
        calendarView.setFirstDayOfWeek(Calendar.MONDAY);
        DateListener listener = new DateListener(requireActivity().getSupportFragmentManager(), getContext());
        calendarView.setOnDateChangeListener(listener);
        this.worker  = new ApiWorker(ApiConsts.url, getContext());
        this.vv = new PopupView(getContext());
        binding.button1.setOnClickListener(this::addCompany);
        return binding.getRoot();
    }


    /**
     * The addCompany function is called when the user clicks on the &quot;Add Company&quot; button.
     * It creates a new popup window that allows the user to select from a list of companies
     * and add them to their portfolio. The function also calls getAvailableCompanies, which
     * queries our database for all available companies in our database. This information is then passed into showPopup, which displays this information in a dropdown menu for users to select from.

     *
     * @param v Show the popup window
     *
     * @docauthor zayycev22
     */
    private void addCompany(View v) {
        Log.d("DEBUG", "Add company");
        View parentView = requireView().findViewById(R.id.home_fragment);
        this.vv.showPopup(parentView);
        this.worker.getAvailableCompanies(this);


    }

    @Override
    public void onRequestSuccess(String answer) {

    }

    /**
     * The onRequestSuccess function is called when the request to the server was successful.
     * It takes an ArrayList of Companies and a String as parameters, which are used to update
     * the RecyclerView in CompanyFragment.java with new data from the server. The function also
     * displays a Toast message on screen with information about what happened during this process.

     *
     * @param companies Update the recyclerview
    ```

    ## license

        copyright 2018 jens kristian villadsen, alexander bjerregaard jørgensen &amp; kasper schou hansen

        licensed under the apache license, version 2
     * @param answer Display a message to the user
     *
     * @docauthor zayycev22
     */
    @Override
    public void onRequestSuccess(ArrayList<Company> companies, String answer) {
        Toast.makeText(getContext(), answer, Toast.LENGTH_SHORT).show();
        this.vv.updateRecyclerView(companies);
    }

    @Override
    public void onRequestSuccess(User user, String answer) {

    }

    @Override
    public void onRequestSuccess(ArrayList<Company> companies) {

    }

    @Override
    public void onRequestFailed(String answer) {
        Toast.makeText(getContext(), answer, Toast.LENGTH_SHORT).show();
    }
}