package com.zayycev22.dividends.home.Calendar;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.zayycev22.dividends.R;
import com.zayycev22.dividends.databinding.FragmentEmptyCalendarDataBinding;


public class EmptyCalendarData extends Fragment {

    private FragmentEmptyCalendarDataBinding binding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    public EmptyCalendarData(){

    }
    public EmptyCalendarData(String text){

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.binding = FragmentEmptyCalendarDataBinding.inflate(inflater, container, false);

        return this.binding.getRoot();
    }
}