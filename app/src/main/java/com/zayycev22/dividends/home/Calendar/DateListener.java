package com.zayycev22.dividends.home.Calendar;

import android.content.Context;
import android.icu.util.Calendar;
import android.util.Log;
import android.widget.CalendarView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.zayycev22.dividends.R;
import com.zayycev22.dividends.api.ApiWorker;
import com.zayycev22.dividends.api.data.Company;
import com.zayycev22.dividends.api.data.User;
import com.zayycev22.dividends.api.user.interfaces.OnRequestCompleted;
import com.zayycev22.dividends.consts.ApiConsts;
import com.zayycev22.dividends.home.Calendar.CalendarData;

import java.util.ArrayList;

public class DateListener implements CalendarView.OnDateChangeListener, OnRequestCompleted {
    private final FragmentManager manager;
    private ApiWorker worker;

    public DateListener(FragmentManager manager, Context context) {
        this.manager = manager;
        this.worker = new ApiWorker(ApiConsts.url, context);
    }

    // todo Получение данных с сервера

    @Override
    public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        int currentYear = calendar.get(Calendar.YEAR);
        int currentMonth = calendar.get(Calendar.MONTH);
        int currentDay = calendar.get(Calendar.DAY_OF_MONTH);
        if (year != currentYear || month != currentMonth || dayOfMonth != currentDay) {
            // выбранная дата не является текущей, устанавливаем текущую дату
            view.setDate(calendar.getTimeInMillis());
        }
        Log.d("month", Integer.toString(month));
        this.worker.checkDateCall(this, month + 1, dayOfMonth);

    }


    public static CalendarData createFragment(ArrayList<Company> companies) {
        return new CalendarData(companies);
    }

    public static EmptyCalendarData createFragment(){
        return new EmptyCalendarData("Empty data");
    }

    @Override
    public void onRequestSuccess(String answer) {

    }

    @Override
    public void onRequestSuccess(ArrayList<Company> companies, String answer) {
        FragmentTransaction fragmentTransaction = manager.beginTransaction();
        Fragment existingFragment = manager.findFragmentById(R.id.calendar_data);
        if (existingFragment != null) {
            fragmentTransaction.remove(existingFragment);
        }

        fragmentTransaction.add(R.id.calendar_data, createFragment(companies));
        fragmentTransaction.commit();
    }

    @Override
    public void onRequestSuccess(User user, String answer) {

    }

    @Override
    public void onRequestSuccess(ArrayList<Company> companies) {

    }

    @Override
    public void onRequestFailed(String answer) {
        FragmentTransaction fragmentTransaction = manager.beginTransaction();
        Fragment existingFragment = manager.findFragmentById(R.id.calendar_data);
        if (existingFragment != null) {
            fragmentTransaction.remove(existingFragment);
        }
        fragmentTransaction.add(R.id.calendar_data, createFragment());
        fragmentTransaction.commit();
    }
}
