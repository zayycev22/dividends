package com.zayycev22.dividends.home.Calendar;

import android.content.res.Configuration;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nex3z.flowlayout.FlowLayout;
import com.zayycev22.dividends.R;
import com.zayycev22.dividends.api.data.Company;
import com.zayycev22.dividends.databinding.FragmentCalendarDataBinding;

import java.util.ArrayList;
import java.util.Locale;

public class CalendarData extends Fragment {

    private FragmentCalendarDataBinding binding;
    private Double sum;
    private ArrayList<Company> companies;
    private int resource_id;

    public CalendarData() {

    }

    public CalendarData(ArrayList<Company> companies) {
        this.companies = companies;
        this.sum =  0.0;
    }

    public CalendarData(ArrayList<Company> companies, Double sum) {
        this.companies = companies;
        this.sum = sum;
    }

    public void setSum() {
        for (Company company : companies){
            this.sum += company.equity_amount * company.equity_price * (company.company_div / 100);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.binding = FragmentCalendarDataBinding.inflate(inflater, container, false);
        this.setResource_id();
        this.setSum();
        this.binding.moneySum.setText(String.format(Locale.US, "%.1f", this.sum));
        FlowLayout flow = binding.companyFlow;
        for (Company company : this.companies) {
            TextView textView = new TextView(getContext());
            textView.setText(company.name);
            textView.setBackgroundResource(this.resource_id);
            textView.setPadding(16, 8, 16, 8);
            FlowLayout.LayoutParams params = new FlowLayout.LayoutParams(
                    FlowLayout.LayoutParams.WRAP_CONTENT,
                    FlowLayout.LayoutParams.WRAP_CONTENT);
            flow.addView(textView, params);
        }
        return this.binding.getRoot();
    }

    private void setResource_id() {
        Log.d("THEME", "Taking");
        int nightMode = AppCompatDelegate.getDefaultNightMode();
        switch (nightMode) {
            case AppCompatDelegate.MODE_NIGHT_YES:
                // Темная тема включена, используйте темные цвета
                this.resource_id = R.drawable.black_flow_corner_item;
                Log.d("THEME", "Dark");
                break;
            case AppCompatDelegate.MODE_NIGHT_NO:
                // Темная тема выключена, используйте светлые цвета
                resource_id = R.drawable.white_flow_corner_item;
                Log.d("THEME", "Light");
                break;
            case AppCompatDelegate.MODE_NIGHT_AUTO_BATTERY:
                Log.d("THEME", "HERE3");
                break;
            case AppCompatDelegate.MODE_NIGHT_AUTO_TIME:
                break;
            case AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM:
                Log.d("THEME", "System");
                break;
            case AppCompatDelegate.MODE_NIGHT_UNSPECIFIED:
                Log.d("THEME", "HERE2");
                this.getUiMode();
                break;
        }
    }

    private void getUiMode() {
        int currentNightMode = getResources().getConfiguration().uiMode
                & Configuration.UI_MODE_NIGHT_MASK;
        if (currentNightMode == Configuration.UI_MODE_NIGHT_YES) {
            // Текущая конфигурация устройства имеет темную тему
            this.resource_id = R.drawable.black_flow_corner_item;
        } else {
            // Текущая конфигурация устройства имеет светлую тему
            this.resource_id = R.drawable.white_flow_corner_item;
        }
    }
}