package com.zayycev22.dividends.home;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.zayycev22.dividends.R;
import com.zayycev22.dividends.api.ApiWorker;
import com.zayycev22.dividends.api.data.Company;
import com.zayycev22.dividends.api.data.User;
import com.zayycev22.dividends.api.user.interfaces.OnRequestCompleted;
import com.zayycev22.dividends.consts.ApiConsts;
import com.zayycev22.dividends.databinding.PopupCompanyBinding;

import java.util.ArrayList;
import java.util.HashSet;

public class PopupView extends View  implements OnRequestCompleted {

    private PopupWindow popupWindow;
    private RecyclerView recyclerView;
    private ProgressBar loadingProgressBar;
    private ArrayList<Company> companies;
    private final PopupCompanyBinding binding;

    private ApiWorker worker;

    public PopupView(Context context) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.binding = PopupCompanyBinding.inflate(inflater, null, false);
        init();
    }

    public PopupView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.binding = PopupCompanyBinding.inflate(inflater, null, false);
        init();
    }

    public PopupView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.binding = PopupCompanyBinding.inflate(inflater, null, false);
        init();
    }

    private void init() {
        // Загрузить макет popup_company.xml

        // Инициализировать элементы PopupWindow
        popupWindow = new PopupWindow(this.binding.getRoot(), ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, true);
        recyclerView = binding.recycleview;
        loadingProgressBar = binding.loading;
        popupWindow.setAnimationStyle(R.style.PopupAnimation);

        // Настройка RecyclerView
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);
        this.binding.cancelButton.setOnClickListener(v -> this.dismiss());
        this.binding.applyButton.setOnClickListener(v -> this.apply());
        this.worker  = new ApiWorker(ApiConsts.url, getContext());
    }


    // Отображение PopupWindow
    public void showPopup(View parentView) {
        popupWindow.showAtLocation(parentView, Gravity.CENTER, 0, 0);
    }

    // Обновление элементов PopupWindow
    public void updateRecyclerView(ArrayList<Company> companies) {
        this.companies = companies;
        recyclerView.setAdapter(new CompanyAdapter(companies));
        loadingProgressBar.setVisibility(View.GONE);
        binding.applyButton.setEnabled(true);
        binding.cancelButton.setEnabled(true);
    }

    public void apply() {
        HashSet<Company> companySet = new HashSet<>();
        for (Company c : this.companies) {
            if (c.checked)
                companySet.add(c);
        }
        Log.d("Company", String.valueOf(companySet.size()));
        this.worker.UpdateCompaniesCall(companySet, this);
    }



    @Override
    public void onRequestSuccess(String answer){
        Toast.makeText(getContext(), answer, Toast.LENGTH_SHORT).show();
        this.dismiss();
    }

    @Override
    public void onRequestSuccess(ArrayList<Company> companies, String answer) {

    }

    @Override
    public void onRequestSuccess(User user, String answer) {

    }

    @Override
    public void onRequestSuccess(ArrayList<Company> companies) {

    }

    @Override
    public void onRequestFailed(String answer) {
        Toast.makeText(getContext(), answer, Toast.LENGTH_SHORT).show();
    }

    public void dismiss() {
        this.popupWindow.dismiss();
    }
}
