package com.zayycev22.dividends.home;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.zayycev22.dividends.api.data.Company;

import com.zayycev22.dividends.databinding.CompanyListBinding;

import java.util.ArrayList;

public class CompanyAdapter extends RecyclerView.Adapter<CompanyAdapter.CompanyHolder> {
    ArrayList<Company> companies;


    public CompanyAdapter(ArrayList<Company> companies) {
        this.companies = companies;
    }

    @NonNull
    @Override
    public CompanyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        CompanyListBinding binding = CompanyListBinding.inflate(inflater, parent, false);
        return new CompanyHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(@NonNull CompanyHolder holder, int position) {
        holder.binding.setCompany(companies.get(position));
        holder.binding.executePendingBindings();

        holder.binding.companyCheckbox.setChecked(companies.get(position).checked);
        holder.binding.companyCheckbox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            companies.get(position).setChecked(isChecked);
        });
        holder.binding.executePendingBindings();
        Log.d("TST", Integer.toString(position));
    }

    @Override
    public int getItemCount() {
        return this.companies.size();
    }

    static class CompanyHolder extends RecyclerView.ViewHolder {
        ;
        CompanyListBinding binding;
        public CompanyHolder(@NonNull View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }
}
