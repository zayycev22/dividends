package com.zayycev22.dividends.account;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;

import com.zayycev22.dividends.R;
import com.zayycev22.dividends.account.iterfaces.OnCompanyRemovedListener;
import com.zayycev22.dividends.api.ApiWorker;
import com.zayycev22.dividends.api.data.Company;
import com.zayycev22.dividends.api.data.User;
import com.zayycev22.dividends.api.user.interfaces.OnRequestCompleted;
import com.zayycev22.dividends.consts.ApiConsts;
import com.zayycev22.dividends.databinding.PopupCompanyDataBinding;

import java.util.ArrayList;
import java.util.Locale;

public class CompanyData extends View implements OnRequestCompleted {

    private final Company company;
    private final @NonNull PopupCompanyDataBinding binding;
    private PopupWindow popupWindow;
    public CompanyViewModel viewModel;
    private ApiWorker worker;
    private OnCompanyRemovedListener listener;

    public CompanyData(Context context) {
        super(context);
        this.company = null;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.binding = PopupCompanyDataBinding.inflate(inflater, null, false);
    }

    public CompanyData(Context context, Company company, OnCompanyRemovedListener listener) {
        super(context);
        this.company = company;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.binding = PopupCompanyDataBinding.inflate(inflater, null, false);
        this.worker = new ApiWorker(ApiConsts.url, context);
        viewModel = new ViewModelProvider((ViewModelStoreOwner) context).get(CompanyViewModel.class);
        viewModel.setFirstState(company.equity_amount);
        this.listener = listener;// устанавливаем начальное состояние во ViewModel
    }

    public void init() {
        this.popupWindow = new PopupWindow(this.binding.getRoot(), ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, true);

        popupWindow.setAnimationStyle(R.style.PopupAnimation);

        this.binding.companyName.setText(this.company.name);
        this.binding.companyPercent.setText(String.format(Locale.US, "%.2f", this.company.company_div));
        this.binding.companyPrice.setText(String.format(Locale.US, "%.2f", this.company.equity_price));
        this.binding.companyAmount.setText(String.format(Locale.US, "%d", this.company.equity_amount));
        this.binding.cancelButton.setOnClickListener(v -> dismiss());
        this.binding.submitButton.setOnClickListener(this::apply);
        this.binding.deleteBtn.setOnClickListener(this::deleteCompany);

        this.binding.companyAmount.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    int number = Integer.parseInt(s.toString());
                    viewModel.updateCompanyAmount(number);
                } catch (NumberFormatException ex) {
                    ex.printStackTrace();
                    Log.d("compdata", "error");
                    viewModel.updateCompanyAmount(-1);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        this.viewModel.getIsButtonEnabled().observe((LifecycleOwner) this.getContext(), this.binding.submitButton::setEnabled);

    }

    public void showPop(View parentView) {
        popupWindow.showAtLocation(parentView, Gravity.CENTER, 0, 0);
    }

    public void dismiss() {
        this.popupWindow.dismiss();
    }

    public void apply(View v) {
        Integer amount = Integer.valueOf(this.binding.companyAmount.getText().toString());
        this.company.setEquity_amount(amount);
        this.worker.updateCompanyCall(this, this.company);
    }

    public void deleteCompany(View v){

        this.worker.removeCompanyCall(this, this.listener, this.company);
    }


    @Override
    public void onRequestSuccess(String answer) {
        Context context = getContext();
        Toast.makeText(context, answer, Toast.LENGTH_SHORT).show();
        this.dismiss();
    }

    @Override
    public void onRequestSuccess(ArrayList<Company> companies, String answer) {

    }

    @Override
    public void onRequestSuccess(User user, String answer) {

    }

    @Override
    public void onRequestSuccess(ArrayList<Company> companies) {

    }

    @Override
    public void onRequestFailed(String answer) {

    }
}
