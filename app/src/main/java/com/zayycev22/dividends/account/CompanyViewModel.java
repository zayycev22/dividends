package com.zayycev22.dividends.account;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class CompanyViewModel extends ViewModel {
    private MutableLiveData<Integer> companyAmount = new MutableLiveData<>();
    private MutableLiveData<Boolean> isButtonEnabled = new MutableLiveData<>();

    private int firstState = -1;

    public void setFirstState(int firstState) {
        this.firstState = firstState;
        isButtonEnabled.setValue(false);
        this.companyAmount.setValue(firstState);
    }


    public LiveData<Boolean> getIsButtonEnabled() {
        return isButtonEnabled;
    }

    public void updateCompanyAmount(int amount) {
        if (amount < 0) {
            amount = 0;
        }
        companyAmount.setValue(amount);
        if (firstState != -1 && amount != firstState && amount != 0) {
            isButtonEnabled.setValue(true);
        } else {
            isButtonEnabled.setValue(false);
        }
    }

    public void checkButtonEnabled(int firstState) {
        this.firstState = firstState;
        if (companyAmount.getValue() != null && companyAmount.getValue() != firstState) {
            isButtonEnabled.postValue(true);
        } else {
            isButtonEnabled.postValue(false);
        }
    }
}
