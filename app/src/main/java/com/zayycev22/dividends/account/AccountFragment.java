package com.zayycev22.dividends.account;

import android.content.res.Configuration;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nex3z.flowlayout.FlowLayout;
import com.zayycev22.dividends.R;

import com.zayycev22.dividends.account.iterfaces.OnCompanyRemovedListener;
import com.zayycev22.dividends.api.ApiWorker;
import com.zayycev22.dividends.api.data.Company;
import com.zayycev22.dividends.api.data.User;
import com.zayycev22.dividends.api.user.interfaces.OnRequestCompleted;
import com.zayycev22.dividends.consts.ApiConsts;
import com.zayycev22.dividends.databinding.FragmentAccountBinding;
import com.zayycev22.dividends.home.PopupView;

import java.util.ArrayList;

public class AccountFragment extends Fragment implements OnRequestCompleted, OnCompanyRemovedListener {
    private FragmentAccountBinding binding;
    private int resource_id;

    private ApiWorker worker;
    private CompanyData data;
    private ArrayList<Company> companies;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentAccountBinding.inflate(inflater, container, false);
        this.worker = new ApiWorker(ApiConsts.url, getContext());
        this.worker.getUserCompanies(this);
        return binding.getRoot();
    }


    private void setCompanies(ArrayList<Company> companies) {
        this.setResource_id();
        FlowLayout flow = binding.flowLayout;
        for (Company company : companies) {
            TextView textView = new TextView(getContext());
            textView.setText(company.name);
            textView.setOnClickListener(this::textClick);
            textView.setBackgroundResource(this.resource_id);
            textView.setPadding(20, 8, 20, 8);
            FlowLayout.LayoutParams params = new FlowLayout.LayoutParams(
                    FlowLayout.LayoutParams.WRAP_CONTENT,
                    FlowLayout.LayoutParams.WRAP_CONTENT);
            flow.addView(textView, params);
        }
    }

    private void removeCompanyFromLayout(Company company) {
        // Получаем индекс компании в списке
        int index = companies.indexOf(company);
        // Получаем индекс соответствующего TextView в FlowLayout
        FlowLayout flow = binding.flowLayout;
        TextView textView = (TextView) flow.getChildAt(index);
        // Удаляем TextView из FlowLayout
        flow.removeView(textView);
        // Удаляем компанию из списка
        companies.remove(company);
    }

    private void setResource_id() {
        Log.d("THEME", "Taking");
        int nightMode = AppCompatDelegate.getDefaultNightMode();
        switch (nightMode) {
            case AppCompatDelegate.MODE_NIGHT_YES:
                // Темная тема включена, используйте темные цвета
                this.resource_id = R.drawable.black_flow_corner_item;
                Log.d("THEME", "Dark");
                break;
            case AppCompatDelegate.MODE_NIGHT_NO:
                // Темная тема выключена, используйте светлые цвета
                resource_id = R.drawable.white_flow_corner_item;
                Log.d("THEME", "Light");
                break;
            case AppCompatDelegate.MODE_NIGHT_AUTO_BATTERY:
                break;
            case AppCompatDelegate.MODE_NIGHT_AUTO_TIME:
                break;
            case AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM:
                break;
            case AppCompatDelegate.MODE_NIGHT_UNSPECIFIED:
                this.getUiMode();
                break;
        }
    }

    private void getUiMode() {
        int currentNightMode = getResources().getConfiguration().uiMode
                & Configuration.UI_MODE_NIGHT_MASK;
        if (currentNightMode == Configuration.UI_MODE_NIGHT_YES) {
            // Текущая конфигурация устройства имеет темную тему
            this.resource_id = R.drawable.black_flow_corner_item;
        } else {
            // Текущая конфигурация устройства имеет светлую тему
            this.resource_id = R.drawable.white_flow_corner_item;
        }
    }

    @Override
    public void onRequestSuccess(String answer) {

    }

    @Override
    public void onRequestSuccess(ArrayList<Company> companies, String answer) {
        this.setCompanies(companies);

    }

    @Override
    public void onRequestSuccess(User user, String answer) {
        this.setCompanies(user.getCompanies());
        this.binding.userView.setText(user.getUsername());
        this.companies = user.getCompanies();
    }

    @Override
    public void onRequestSuccess(ArrayList<Company> companies) {

    }

    @Override
    public void onRequestFailed(String answer) {

    }

    public void textClick(View v) {
        if (this.companies != null) {
            String company_name = ((TextView) v).getText().toString();
            for (Company c : this.companies) {
                if (c.getName().equals(company_name)) {
                    this.data = new CompanyData(getContext(), c, this);
                    break;
                }
            }
            data.init();
            View parentView = requireView().findViewById(R.id.account_fragment);
            data.showPop(parentView);
        }
    }

    @Override
    public void onCompanyRemoved(Company company) {
        this.removeCompanyFromLayout(company);
    }
}