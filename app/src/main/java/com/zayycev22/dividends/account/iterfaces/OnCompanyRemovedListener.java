package com.zayycev22.dividends.account.iterfaces;

import com.zayycev22.dividends.api.data.Company;

public interface OnCompanyRemovedListener {
    void onCompanyRemoved(Company company);
}
