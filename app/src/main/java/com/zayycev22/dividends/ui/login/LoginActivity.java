package com.zayycev22.dividends.ui.login;

import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.zayycev22.dividends.MainActivity;
import com.zayycev22.dividends.api.ApiWorker;
import com.zayycev22.dividends.api.auth.interfaces.OnAuthCompleted;
import com.zayycev22.dividends.consts.ApiConsts;
import com.zayycev22.dividends.databinding.ActivityLoginBinding;


public class LoginActivity extends AppCompatActivity implements OnAuthCompleted {

    private ActivityLoginBinding binding;
    private LoginViewModel loginViewModel;
    private ApiWorker worker;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        loginViewModel = new ViewModelProvider(this)
                .get(LoginViewModel.class);


        final EditText usernameEditText = binding.username;
        final EditText passwordEditText = binding.password;
        final Button loginButton = binding.login;
        final Button registerButton = binding.register;

        //String url = System.getenv("URL");
        // Log.d("URL", url);

        worker = new ApiWorker(ApiConsts.url, getApplicationContext()); //Ссылка на restApi


        usernameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Not used
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                loginViewModel.setLogin(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                // Not used
            }
        });

        passwordEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Not used
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                loginViewModel.setPassword(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                // Not used
            }
        });

        loginViewModel.isFormValid().observe(this, formValid -> {
            loginButton.setEnabled(formValid);
            registerButton.setEnabled(formValid);
        });

        loginButton.setOnClickListener(this::loginButton);
        registerButton.setOnClickListener(this::registerButton);
    }

    private void unclickableButtons() {
        this.binding.login.setEnabled(false);
        this.binding.register.setEnabled(false);
    }

    private void editableText(boolean editable) {
        this.binding.username.setEnabled(editable);
        this.binding.password.setEnabled(editable);
    }

    private void deleteText() {
        this.binding.username.setText("");
        this.binding.password.setText("");
    }

    private void refresh() {
        this.unclickableButtons();
        this.editableText(true);
        this.binding.loading.setVisibility(View.GONE);
        this.deleteText();
    }

    private void loginButton(View v) {
        this.binding.loading.setVisibility(View.VISIBLE);
        this.unclickableButtons();
        this.editableText(false);
        worker.LoginCall(this.binding.username.getText().toString(), this.binding.password.getText().toString(), this);
    }

    private void registerButton(View v) {
        this.binding.loading.setVisibility(View.VISIBLE);
        this.unclickableButtons();
        this.editableText(false);
        worker.RegisterCall(this.binding.username.getText().toString(), this.binding.password.getText().toString(), this);
    }


    @Override
    public void onAuthSuccess(String answer) {
        Context context = getApplicationContext();
        Toast.makeText(context, answer, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent); //todo добавить проверку на токен в mainactivity
        finish();
    }

    @Override
    public void onAuthFailed(String answer) {
        Context context = getApplicationContext();
        Toast.makeText(context, answer, Toast.LENGTH_SHORT).show();
        this.refresh();

    }
}