package com.zayycev22.dividends.ui.login;

import android.text.TextUtils;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import com.zayycev22.dividends.databinding.ActivityLoginBinding;

public class LoginViewModel extends ViewModel {

    private MutableLiveData<String> login = new MutableLiveData<>();
    private MutableLiveData<String> password = new MutableLiveData<>();
    private MutableLiveData<Boolean> isLoginValid = new MutableLiveData<>();
    private MutableLiveData<Boolean> isPasswordValid = new MutableLiveData<>();

    public LiveData<Boolean> isFormValid() {
        MediatorLiveData<Boolean> formValid = new MediatorLiveData<>();
        formValid.addSource(login, login -> {
            boolean isValid = !TextUtils.isEmpty(login) && login.length() >= 3;
            formValid.setValue(isValid && password.getValue() != null && password.getValue().length() >= 8);
        });
        formValid.addSource(password, password -> {
            boolean isValid = !TextUtils.isEmpty(password) && password.length() >= 8;
            formValid.setValue(isValid && login.getValue() != null && login.getValue().length() >= 3);
        });
        return formValid;
    }

    public LiveData<Boolean> isLoginValid() {
        return isLoginValid;
    }

    public LiveData<Boolean> isPasswordValid() {
        return isPasswordValid;
    }

    public void setLogin(String login) {
        isLoginValid.setValue(!TextUtils.isEmpty(login));
        this.login.setValue(login);
    }

    public void setPassword(String password) {
        boolean passwordIsValid = !TextUtils.isEmpty(password) && password.length() >= 8;
        if (Boolean.TRUE.equals(isPasswordValid.getValue()) == passwordIsValid) {
            isPasswordValid.setValue(passwordIsValid);
        }
        this.password.setValue(password);
    }
}
