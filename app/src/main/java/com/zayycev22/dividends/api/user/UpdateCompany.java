package com.zayycev22.dividends.api.user;

import android.util.Log;

import androidx.annotation.NonNull;

import com.zayycev22.dividends.api.auth.interfaces.OnAuthCompleted;
import com.zayycev22.dividends.api.user.interfaces.OnRequestCompleted;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateCompany implements Callback<Void> {

    private String text_answer;
    private OnRequestCompleted onRequestCompleted;

    public UpdateCompany(){
        this.text_answer = "Error happened";
    }

    public void setOnRequestCompleted(OnRequestCompleted onRequestCompleted){
        this.onRequestCompleted = onRequestCompleted;
    }

    @Override
    public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
        if (response.isSuccessful()) {

            this.text_answer = "Company updated";
            onRequestCompleted.onRequestSuccess(this.text_answer); //todo change company data
        } else {
            onRequestCompleted.onRequestFailed(this.text_answer);
        }
    }

    @Override
    public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {
        onRequestCompleted.onRequestSuccess(this.text_answer);
    }
}
