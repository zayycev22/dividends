package com.zayycev22.dividends.api.user;

import androidx.annotation.NonNull;

import com.zayycev22.dividends.api.data.Company;
import com.zayycev22.dividends.api.data.User;
import com.zayycev22.dividends.api.user.interfaces.OnRequestCompleted;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserDataCallback implements Callback<User> {

    private OnRequestCompleted onRequestCompleted;
    private String answer;

    public UserDataCallback(){
        this.answer = "Error happened";
    }

    public void setOnRequestCompleted(OnRequestCompleted onRequestCompleted) {
        this.onRequestCompleted = onRequestCompleted;
    }


    @Override
    public void onResponse(@NonNull Call<User> call, Response<User> response) {
        if (response.isSuccessful()) {
            User user = response.body();
            this.answer = "Completed";
            onRequestCompleted.onRequestSuccess(user, this.answer);
        } else {
            onRequestCompleted.onRequestFailed(this.answer);
        }
    }

    @Override
    public void onFailure(@NonNull Call<User> call, @NonNull Throwable t) {
        onRequestCompleted.onRequestFailed(this.answer);
    }
}
