package com.zayycev22.dividends.api.user;

import android.util.Log;

import androidx.annotation.NonNull;

import com.zayycev22.dividends.api.auth.interfaces.OnAuthCompleted;
import com.zayycev22.dividends.api.data.Company;
import com.zayycev22.dividends.api.user.interfaces.OnRequestCompleted;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Callback;

public class CompanyAvailableCallback implements Callback<ArrayList<Company>>{


    private boolean isSuccess;

    private String text_answer;
    private OnRequestCompleted onRequestCompleted;

    public void setOnRequestCompleted(OnRequestCompleted onRequestCompleted) {
        this.onRequestCompleted = onRequestCompleted;
    }

    public CompanyAvailableCallback() {
        this.isSuccess = false;
        this.text_answer = "Error happened";
    }

    @Override
    public void onResponse(@NonNull Call<ArrayList<Company>> call, @NonNull Response<ArrayList<Company>> response) {
        if (response.isSuccessful()) {
            ArrayList<Company> companies = response.body();
            this.isSuccess = true;
            this.text_answer = "Completed";
            onRequestCompleted.onRequestSuccess(companies, this.text_answer);
        } else {
            this.isSuccess = false;
            onRequestCompleted.onRequestFailed(this.text_answer);
        }
    }

    @Override
    public void onFailure(@NonNull Call<ArrayList<Company>> call, @NonNull Throwable t) {
        Log.d("Company", t.toString());
        this.isSuccess = false;
    }

}
