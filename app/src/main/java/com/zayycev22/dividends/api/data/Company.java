package com.zayycev22.dividends.api.data;

public class Company {
    public String name;
    public Integer equity_amount;
    public Double equity_price;
    public Double company_div;

    public boolean checked;

    public Company(String name) {
        this.name = name;
        this.equity_price = 1440.0;
        this.company_div = 4.3;
        this.equity_amount = 3;
    }

    public Company(String name, Integer equity_amount, Double equity_price, Double company_div) {
        this.name = name;
        this.equity_amount = equity_amount;
        this.equity_price = equity_price;
        this.company_div = company_div;
        this.checked = false;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getEquity_amount() {
        return equity_amount;
    }

    public void setEquity_amount(Integer equity_amount) {
        this.equity_amount = equity_amount;
    }

    public Double getEquity_price() {
        return equity_price;
    }

    public void setEquity_price(Double equity_price) {
        this.equity_price = equity_price;
    }

    public Double getCompany_div() {
        return company_div;
    }

    public void setCompany_div(Double company_div) {
        this.company_div = company_div;
    }

    public Double getIncome(){
        return this.equity_amount * this.equity_price * this.company_div / 100;
    }
}
