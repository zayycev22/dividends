package com.zayycev22.dividends.api.user;

import com.google.gson.annotations.SerializedName;

public class CheckDateRequest {
    @SerializedName("day")
    private int day;
    @SerializedName("month")
    private int month;

    public CheckDateRequest(int day, int month) {
        this.day = day;
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }
}
