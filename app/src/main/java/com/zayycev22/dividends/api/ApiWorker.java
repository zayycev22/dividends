package com.zayycev22.dividends.api;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.zayycev22.dividends.account.iterfaces.OnCompanyRemovedListener;
import com.zayycev22.dividends.api.auth.LoginCallback;
import com.zayycev22.dividends.api.auth.LoginRequest;
import com.zayycev22.dividends.api.auth.interfaces.OnAuthCompleted;
import com.zayycev22.dividends.api.auth.RegisterCallback;
import com.zayycev22.dividends.api.auth.RegisterRequest;
import com.zayycev22.dividends.api.data.Company;
import com.zayycev22.dividends.api.data.User;
import com.zayycev22.dividends.api.user.CheckDateCallback;
import com.zayycev22.dividends.api.user.CheckDateRequest;
import com.zayycev22.dividends.api.user.CompanyAvailableCallback;
import com.zayycev22.dividends.api.user.CompanyUpdateCallback;
import com.zayycev22.dividends.api.user.RemoveCompanyCallback;
import com.zayycev22.dividends.api.user.UpdateCompany;
import com.zayycev22.dividends.api.user.UserDataCallback;
import com.zayycev22.dividends.api.user.interfaces.OnRequestCompleted;

import java.util.ArrayList;
import java.util.HashSet;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiWorker {

    private final ApiService apiService;
    private final Context mContext;


    public ApiWorker(String url, Context context) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        this.apiService = retrofit.create(ApiService.class);
        this.mContext = context;

    }


    public void LoginCall(String username, String password, OnAuthCompleted callback2) {
        LoginRequest request = new LoginRequest(username, password);
        Log.d("LOGIN", "HERE");
        Call<ResponseBody> call = apiService.login(request);
        LoginCallback callback = new LoginCallback(this.mContext);
        callback.setOnAuthCompleted(callback2);
        call.enqueue(callback);

    }

    public void RegisterCall(String username, String password, OnAuthCompleted callback2) {
        RegisterRequest request = new RegisterRequest(username, password);
        Log.d("LOGIN", "HERE");
        Call<ResponseBody> call = apiService.register(request);
        RegisterCallback callback = new RegisterCallback(this.mContext);
        callback.setOnAuthCompleted(callback2);
        call.enqueue(callback);
    }

    public String getAuthToken() {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences("auth", Context.MODE_PRIVATE);
        return "Token " + sharedPreferences.getString("token", null);
    }

    public void UpdateCompaniesCall(HashSet<Company> companies, OnRequestCompleted callback2){
        String token = this.getAuthToken();
        Call<Void> call = apiService.update_companies(token, companies);
        CompanyUpdateCallback callback = new CompanyUpdateCallback();
        callback.setOnRequestCompleted(callback2);
        call.enqueue(callback);
    }

    public void getAvailableCompanies(OnRequestCompleted callback2){
        String token =  this.getAuthToken();
        Call<ArrayList<Company>> call = apiService.available_companies(token);
        CompanyAvailableCallback callback = new CompanyAvailableCallback();
        callback.setOnRequestCompleted(callback2);
        call.enqueue(callback);
    }

    public void getUserCompanies(OnRequestCompleted callback2){
        String token =  this.getAuthToken();
        Call<User> call = apiService.user_data(token);
        UserDataCallback callback = new UserDataCallback();
        callback.setOnRequestCompleted(callback2);
        call.enqueue(callback);

    }

    public void updateCompanyCall(OnRequestCompleted callback2, Company company){
        String token =  this.getAuthToken();
        Call<Void> call = apiService.update_company(token, company);
        UpdateCompany callback = new UpdateCompany();
        callback.setOnRequestCompleted(callback2);
        call.enqueue(callback);
    }

    public void removeCompanyCall(OnRequestCompleted request, OnCompanyRemovedListener listener, Company company){
        String token =  this.getAuthToken();
        Call<Void> call = apiService.remove_company(token, company);
        RemoveCompanyCallback callback = new RemoveCompanyCallback(company);
        callback.setOnRequestCompleted(request);
        callback.setListener(listener);
        call.enqueue(callback);
    }

    public void checkDateCall(OnRequestCompleted request, int month, int day){
        String token =  this.getAuthToken();
        CheckDateRequest request1 = new CheckDateRequest(day, month);
        Call<ArrayList<Company>> call = apiService.check_date(token, request1);
        CheckDateCallback callback = new CheckDateCallback();
        callback.setOnRequestCompleted(request);
        call.enqueue(callback);
    }

}
