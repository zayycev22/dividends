package com.zayycev22.dividends.api.auth;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.zayycev22.dividends.api.auth.interfaces.OnAuthCompleted;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginCallback implements Callback<ResponseBody> {

    private final Context mContext;
    private boolean isSuccess;
    private String text_answer;
    private OnAuthCompleted onAuthCompleted;

    public LoginCallback(Context context){
        this.mContext = context;
        this.isSuccess = false;
        this.text_answer = "Error happened";
    }



    public void setOnAuthCompleted(OnAuthCompleted onAuthCompleted) {
        this.onAuthCompleted = onAuthCompleted;
    }

    @Override
    public void onResponse(@NonNull Call<ResponseBody> call, Response<ResponseBody> response) {
        if (response.isSuccessful()) {

            try {
                Gson gson = new Gson();
                assert response.body() != null;
                String serverAnswer = response.body().string();
                JsonObject answer = gson.fromJson(serverAnswer, JsonObject.class);
                this.saveAuthToken(answer.get("token").getAsString());
                Log.d("LOGIN", answer.get("token").getAsString());
                this.text_answer = answer.get("status").getAsString();
                this.isSuccess = true;
                onAuthCompleted.onAuthSuccess(this.text_answer);
            } catch (IOException e) {
                e.printStackTrace();
                this.isSuccess = false;
                onAuthCompleted.onAuthFailed(this.text_answer);
            }
        } else {
            try {
                Gson gson = new Gson();
                assert response.errorBody() != null;
                String serverAnswer = response.errorBody().string();
                JsonObject answer = gson.fromJson(serverAnswer, JsonObject.class);
                this.text_answer = answer.get("status").getAsString();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            this.isSuccess = false;
            onAuthCompleted.onAuthFailed(this.text_answer);
        }
    }

    @Override
    public void onFailure(@NonNull Call call, Throwable t) {
        Log.d("LOGIN", t.toString());
        this.isSuccess = false;
        onAuthCompleted.onAuthFailed(this.text_answer);
    }

    private void saveAuthToken(String token) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences("auth", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("token", token);
        editor.apply();
        this.onAuthCompleted.onAuthSuccess(token);
    }

}
