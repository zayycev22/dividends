package com.zayycev22.dividends.api.data;

import java.util.ArrayList;

public class User {
    private final String username;
    private final ArrayList<Company> user_companies;

    public User(String username, ArrayList<Company> companies) {
        this.username = username;
        this.user_companies = companies;
    }

    public Double getDivSum() {
        Double sum = 0.0;
        for (Company company : this.user_companies){
            sum += company.getIncome();
        }
        return sum;
    }

    public String getUsername(){
        return this.username;
    }

    public ArrayList<Company> getCompanies(){
        return this.user_companies;
    }
}
