package com.zayycev22.dividends.api.user;

import androidx.annotation.NonNull;

import com.zayycev22.dividends.api.data.Company;
import com.zayycev22.dividends.api.user.interfaces.OnRequestCompleted;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckDateCallback implements Callback<ArrayList<Company>> {

    private String text_answer;
    private OnRequestCompleted onRequestCompleted;

    public CheckDateCallback(){
        this.text_answer = "no data";
    }

    public void setOnRequestCompleted(OnRequestCompleted requestCompleted){
        this.onRequestCompleted = requestCompleted;
    }

    @Override
    public void onResponse(@NonNull Call<ArrayList<Company>> call, Response<ArrayList<Company>> response) {
        if (response.isSuccessful()) {
            ArrayList<Company> companies = response.body();
            this.text_answer = "Completed";
            onRequestCompleted.onRequestSuccess(companies, this.text_answer);
        } else {
            onRequestCompleted.onRequestFailed(this.text_answer);
        }
    }

    @Override
    public void onFailure(@NonNull Call<ArrayList<Company>> call, @NonNull Throwable t) {
        onRequestCompleted.onRequestFailed(this.text_answer);
    }
}
