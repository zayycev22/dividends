package com.zayycev22.dividends.api;

import com.zayycev22.dividends.api.data.Company;
import com.zayycev22.dividends.api.auth.LoginRequest;
import com.zayycev22.dividends.api.auth.RegisterRequest;
import com.zayycev22.dividends.api.data.User;
import com.zayycev22.dividends.api.user.CheckDateRequest;

import java.util.ArrayList;
import java.util.HashSet;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;

public interface ApiService {
    @Headers("Content-Type: application/json")
    @POST("/api/v1/login/")
    Call<ResponseBody> login(@Body LoginRequest request);

    @Headers("Content-Type: application/json")
    @POST("/api/v1/reg/")
    Call<ResponseBody> register(@Body RegisterRequest request);

    @GET("/api/v1/available_companies/")
    Call<ArrayList<Company>> available_companies(@Header("Authorization") String token);

    @GET("/api/v1/me/")
    Call<User> user_data(@Header("Authorization") String token);

    @PUT("/api/v1/update_companies/")
    Call<Void> update_companies(@Header("Authorization") String token, @Body HashSet<Company> companies);

    @PUT("/api/v1/update_company/")
    Call<Void> update_company(@Header("Authorization") String token, @Body Company company);

    @PUT("/api/v1/remove_company/")
    Call<Void> remove_company(@Header("Authorization") String token, @Body Company company);

    @POST("/api/v1/check_date/")
    Call<ArrayList<Company>> check_date(@Header("Authorization") String token, @Body CheckDateRequest dateRequest);
}
