package com.zayycev22.dividends.api.auth.interfaces;

public interface OnAuthCompleted {
    void onAuthSuccess(String answer);
    void onAuthFailed(String answer);
}
