package com.zayycev22.dividends.api.user.interfaces;

import com.zayycev22.dividends.api.data.Company;
import com.zayycev22.dividends.api.data.User;

import java.util.ArrayList;

public interface OnRequestCompleted {
    void onRequestSuccess(String answer);

    void onRequestSuccess(ArrayList<Company> companies, String answer);

    void onRequestSuccess(User user, String answer);

    void onRequestSuccess(ArrayList<Company> companies);

    void onRequestFailed(String answer);
}
