package com.zayycev22.dividends.api.user;

import androidx.annotation.NonNull;

import com.zayycev22.dividends.account.iterfaces.OnCompanyRemovedListener;
import com.zayycev22.dividends.api.data.Company;
import com.zayycev22.dividends.api.user.interfaces.OnRequestCompleted;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RemoveCompanyCallback implements Callback<Void> {

    private String text_answer;
    private final Company company;
    private OnRequestCompleted onRequestCompleted;

    private OnCompanyRemovedListener listener;

    public void setListener(OnCompanyRemovedListener listener) {
        this.listener = listener;
    }

    public RemoveCompanyCallback(Company company) {
        this.text_answer = "Error happened";
        this.company = company;
    }

    public void setOnRequestCompleted(OnRequestCompleted onRequestCompleted) {
        this.onRequestCompleted = onRequestCompleted;
    }

    @Override
    public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
        if (response.isSuccessful()) {
            this.text_answer = "Company deleted";
            this.onRequestCompleted.onRequestSuccess(this.text_answer);
            this.listener.onCompanyRemoved(company);
        } else {
            onRequestCompleted.onRequestFailed(this.text_answer);
        }
    }

    @Override
    public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {

    }
}
