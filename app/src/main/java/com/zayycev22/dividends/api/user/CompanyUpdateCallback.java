package com.zayycev22.dividends.api.user;

import android.util.Log;

import androidx.annotation.NonNull;

import com.zayycev22.dividends.api.user.interfaces.OnRequestCompleted;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CompanyUpdateCallback implements Callback<Void> {

    private boolean isSuccess;

    private String text_answer;
    private OnRequestCompleted onAuthCompleted;

    public void setOnRequestCompleted(OnRequestCompleted onAuthCompleted) {
        this.onAuthCompleted = onAuthCompleted;
    }

    public CompanyUpdateCallback() {
        this.isSuccess = false;
        this.text_answer = "Error happened, try again";
    }

    @Override
    public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
        if (response.isSuccessful()) {

            this.isSuccess = true;
            this.text_answer = "Companies added";
            onAuthCompleted.onRequestSuccess(this.text_answer);
        } else {
            this.isSuccess = false;
            onAuthCompleted.onRequestFailed(this.text_answer);
        }
    }

    @Override
    public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {
        Log.d("Company", t.toString());
        this.isSuccess = false;
    }
}
