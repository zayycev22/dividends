package com.zayycev22.dividends;


import com.zayycev22.dividends.api.data.Company;
import com.zayycev22.dividends.api.data.User;

import org.junit.Test;

import java.util.ArrayList;

public class UserTest {
    public static ArrayList<Company> createCompanyList(int count) {
        ArrayList<Company> companies = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            Company c = new Company("Сбербанк");
            companies.add(c);
        }
        return companies;
    }

    @Test
    public void testDivSum() {
        ArrayList<Company> companies = createCompanyList(4);
        User user = new User("zayycev22", companies);
        assert 743.04 == user.getDivSum();
    }
}
